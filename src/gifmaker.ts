import * as Discord from "discord.js";
import { createRequire } from 'module';
const require = createRequire(import.meta.url);
import * as gw from "gif-writer";
class OutputStream implements gw.IOutputStream {
  buffer: number[] = [];
  writeByte(b: number) {
    this.buffer.push(b);
  }
  writeBytes(bb: number[]) {
    Array.prototype.push.apply(this.buffer, bb);
  }
};
const jsdom = require("jsdom");

var html = "<html><body></body></html>";
var options = {
  url: "file://" + process.cwd() + "/",
  features: {
    FetchExternalResources: ["img", "script"],
  },
  resources: "usable",
};

var window: any;
var document: any;

function resetDOM() {
  if (jsdom.JSDOM) {
    var dom = new jsdom.JSDOM(html, options);
    window = dom.window;
  } else {
    document = jsdom.jsdom(html, options);
    window = document.defaultView;
  }
  document = window.document;
}

resetDOM();

const client = new Discord.Client({
  intents: [
    Discord.GatewayIntentBits.Guilds,
    Discord.GatewayIntentBits.GuildMessages,
    Discord.GatewayIntentBits.MessageContent,
  ],
});
var IMG_SIZE = 128;

client.on("ready", () => {
  console.log(`Logged in as ${client?.user?.tag}!`);
});

var workingSpaceElem: HTMLElement;
client.on("messageCreate", async (message) => {
  resetDOM();
  if (message.content === "!gif_channel") {
    const selectMenu = new Discord.ChannelSelectMenuBuilder()
      .addChannelTypes(
        Discord.ChannelType.GuildText,
        Discord.ChannelType.GuildAnnouncement
      )
      .setCustomId("gifmaker");
    const row =
      new Discord.ActionRowBuilder<Discord.ChannelSelectMenuBuilder>().addComponents(
        selectMenu
      );
    await message.reply({
      content: "チャンネルを選択してください",
      components: [row],
    });
  }
  
  if (message.content.startsWith("!gif")) {
    const args = message.content.slice("!gif".length).trim().split(" ");
    message.channel?.send({ content: "here we go..." });

    const channel = client.channels.cache.get(
      "1180481901509750905"
    ) as Discord.TextChannel;

    const messages = await channel?.messages.fetch({
      limit: Number(args[0]) || 20,
    });

    const imageMessages = messages.filter(
      (msg) =>
        msg.attachments.size > 0 &&
        msg.attachments.first()?.contentType?.startsWith("image/")
    );

    workingSpaceElem = document.createElement("div");
    workingSpaceElem.style.height = "1px";
    workingSpaceElem.style.overflow = "hidden";

    imageMessages.forEach((imgMsg) => {
      // 新しいimg要素を作成
      var imgElement = document.createElement("img");
      document.body.appendChild(imgElement);
      imgElement.src = imgMsg.attachments.first()?.url;
    });

    var imgElems = <HTMLImageElement[]>(
      Array.prototype.slice.call(document.getElementsByTagName("img"))
    );
    const proms = imgElems.map(
      (im) =>
        new Promise((res) => (im.onload = () => res([im.width, im.height])))
    );
    var imageDataList: ImageData[];
    Promise.all(proms).then((data) => {
      imageDataList = imgElems
        .map((e) => {
          return convertImgElemToImgData(e);
        })
        .filter((e) => !!e)
        .reverse();
      if (imageDataList.length === 0) {
        console.log("No image could be loaded...");
        return;
      }
      message.channel?.send({ content: "success, please wait..." });

      var paletteSize = 255;
      var delayTimeInMS = Number(args[1]) || 400;

      var indexedColorImages = imageDataList.map((e: any) =>
        convertImgDataToIndexedColorImage(e, paletteSize)
      );
      var os = new OutputStream();
      var gifWriter = new GifWriter(os);
      gifWriter.writeHeader();
      gifWriter.writeLogicalScreenInfo({ width: IMG_SIZE, height: IMG_SIZE });
      gifWriter.writeLoopControlInfo(0);
      indexedColorImages.forEach((img: any) => {
        gifWriter.writeTableBasedImageWithGraphicControl(img, {
          delayTimeInMS: delayTimeInMS,
        });
      });
      gifWriter.writeTrailer();

      var gifDataStr = os.buffer.map((b) => String.fromCharCode(b)).join("");
      var base64Str = btoa(gifDataStr);
      let buff = Buffer.from(base64Str, "base64");

      const attachment = new Discord.AttachmentBuilder(buff, {
        name: "out.gif",
      });
      message.channel?.send({ files: [attachment] });
    });
  }
});

function convertImgElemToImgData(imgElem: HTMLImageElement): ImageData {
  var canvasElem = document.createElement("canvas");
  canvasElem.style.width = IMG_SIZE + "px";
  canvasElem.style.height = IMG_SIZE + "px";
  workingSpaceElem.appendChild(canvasElem);
  var ctx = canvasElem.getContext("2d");
  if (!ctx) throw new Error();
  ctx.drawImage(imgElem, 0, 0, IMG_SIZE, IMG_SIZE);
  var imgData = ctx.getImageData(0, 0, IMG_SIZE, IMG_SIZE);
  workingSpaceElem.removeChild(canvasElem);
  return imgData;
}

var GifWriter = gw.GifWriter;
var IndexedColorImage = gw.IndexedColorImage;
var MedianCutColorReducer = gw.MedianCutColorReducer;

function convertImgDataToIndexedColorImage(
  imgData: gw.IImageData,
  paletteSize: number
): gw.IndexedColorImage {
  var reducer = new MedianCutColorReducer(imgData, paletteSize);
  var paletteData = reducer.process();
  var dat = Array.prototype.slice.call(imgData.data);
  var indexedColorImageData: number[] = [];
  for (var idx = 0, len = dat.length; idx < len; idx += 4) {
    var d = dat.slice(idx, idx + 4); // r,g,b,a
    indexedColorImageData.push(reducer.map(d[0], d[1], d[2]));
  }
  return new IndexedColorImage(
    { width: imgData.width, height: imgData.height },
    indexedColorImageData,
    paletteData
  );
}

client.login(
  "MTE4NzM0MDQ4MzgyODEyOTgxMw.Gn1vMr.193ZaecPUZoYZxcuKTfIOK-LPpeCD5JrrRhxV4"
);